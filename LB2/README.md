Inhaltsverzeichnis (Punkte 2-6 unten müssen vorhanden sein)

Einleitung (Einführende Erklärungen zum Projekt)

Service-Beschreibung (Wozu dient die Anwendung)

Umsetzung (Eigener Inhalt, keine Vorgaben)

Anwendung (Wie wird der Dienst verfügbar gemacht, wie darauf zugegriffen)

Quellen